Rails.application.routes.draw do
  root 'posts#index'

  get 'signup', to: 'users#new'
  get 'account', to: 'users#show'
  get 'news', to: 'users#news'
  get 'followed/:id', to: 'users#followed', as: 'followed'
  post 'users/create'

  get 'login', to: 'sessions#new'
  post 'sessions/create'
  delete 'sessions/destroy', as: :logout

  get 'posts/index'
  resources :posts do
    resources :comments, only: [:create]
  end

  post 'subscriptions/create'
  delete 'subscriptions/destroy'
end
