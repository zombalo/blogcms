class PostsController < ApplicationController
  before_action :accept_logged, only: [:new, :create, :edit, :update, :destroy]

  def index
    @posts = Post.all
  end

  def new
    @post = current_user.posts.new
  end

  def create
    @post = current_user.posts.create(params.require(:post).permit(:title, :content))

    if @post.save
      redirect_to account_url
    elsif
      respond_to do |format|
        format.js { render partial: '/show_errors', locals: { object: @post } }
      end
    end
  end

  def show
    @post = Post.find(params[:id])
  end

  def edit
    @post = current_user.posts.find(params[:id])
  end

  def update
    @post = current_user.posts.find(params[:id])

    if @post.update(params.require(:post).permit(:title, :content))
      flash[:success] = 'Post successfully updated'
      redirect_to account_path
    elsif
      respond_to do |format|
        format.js { render partial: '/show_errors', locals: { object: @post } }
      end
    end
  end

  def destroy
    Post.find(params[:id]).destroy
  end
end