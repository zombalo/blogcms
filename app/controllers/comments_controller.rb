class CommentsController < ApplicationController
  before_action :accept_logged, only: [:create]

  def create
    @post = Post.find(params[:post_id])
    @comment = current_user.comments.create({ comment: params[:comment], post: @post })

    if @comment.save
      respond_to :js
    elsif
      respond_to do |format|
        format.js { render partial: '/show_errors', locals: { object: @comment } }
      end
    end
  end
end
