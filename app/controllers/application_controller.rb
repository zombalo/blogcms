class ApplicationController < ActionController::Base
  helper_method [ :current_user, :post_date]

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def accept_logged
    if current_user.nil?
      puts 'unlogged'
      redirect_to login_path
      return false
    end
  end

  def post_date (post)
    datetime = Time.at(post.created_at).to_datetime
    time = datetime.to_time
    date = datetime.to_date
    return date.day.to_s + '/' + date.month.to_s + '/' + date.year.to_s + ' ' + time.hour.to_s + ':' + time.min.to_s + ':' + time.sec.to_s
  end
end
