class SessionsController < ApplicationController
  before_action :accept_logged, only: [:destroy]

  def new
    render 'users/login'
  end

  def create
    @user = User.find_by email: params[:auth][:email].downcase

    if !@user.nil? && BCrypt::Password.new(@user.password_digest) == params[:auth][:password]
      session[:user_id] = @user.id
      redirect_to posts_index_url, flash: { success: 'Login success' }
    elsif
      @user = User.new
      @user.errors[:base].push('Invalid email or password is present');
      respond_to do |format|
        format.js { render partial: '/show_errors', locals: { object: @user } }
      end
    end
  end

  def destroy
    session.delete(:user_id)
    redirect_to root_url, flash: { success: 'You have logged out' }
  end
end
