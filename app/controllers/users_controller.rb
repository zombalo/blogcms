class UsersController < ApplicationController
  before_action :accept_logged, only: [:show, :news, :followed]

  def new
    @user = User.new
  end

  def create
     @user = User.new(params.require(:user).permit(:fname, :lname, :email, :password, :password_confirmation))

     if @user.save
      session[:user_id] = @user.id
      flash[:success] = 'Account created'
      redirect_to posts_index_url
     elsif
      @user.errors.full_messages.each { |message| puts message }
      redirect_to posts_index_url
     end
  end

  def show
    @posts = current_user.posts
  end

  def news
    @posts = current_user.last_posted.sort_by { |post| post.created_at.to_i }.reverse
  end

  def followed
    @posts = Post.where(user_id: params[:id]).all
    @followed = User.find(params[:id])
  end
end
