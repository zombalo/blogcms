class SubscriptionsController < ApplicationController
  before_action :accept_logged, only: [:create, :destroy]

  def create
    @subscription = current_user.subscriptions.create(followed_id: params[:user_id])

    if @subscription.save
      respond_to do |format|
        format.js { render partial: 'subscribe', locals: { action: :unsubscribe, followed_id: params[:user_id] } }
      end
    end
  end

  def destroy
    if current_user.subscriptions.find_by(followed_id: params[:user_id]).destroy
      respond_to do |format|
        format.js { render partial: 'subscribe', locals: { action: :subscribe, followed_id: params[:user_id] } }
      end
    end
  end
end
