class User < ApplicationRecord
  has_many :posts
  has_many :subscriptions, foreign_key: 'follower_id'
  has_many :comments

  has_secure_password

  validates :fname, presence: true
  validates :lname, presence: true
  validates :email, presence: true, format: { with: /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/, message: 'is incorrect' }, uniqueness: true
  validates :password, confirmation: true, length: { minimum: 8 }, presence: true
  validates :password_confirmation, presence: true

  def last_posted
    new_posts = [];
    self.subscriptions.each do |subscription|
      new_posts += subscription.followed.posts
    end

    return new_posts
  end
end
