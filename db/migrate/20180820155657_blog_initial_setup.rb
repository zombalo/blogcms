class BlogInitialSetup < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :email
      t.string :fname
      t.string :lname
      t.string :password_hash
      t.string :password_salt

      t.timestamps
    end

    create_table :posts do |t|
      t.string :title
      t.text :content

      t.timestamps
    end

    add_reference :posts, :user, foreign_key: true
  end
end
