class RememberSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :sessions do |t|
      t.string :agent_token
      t.integer :expires
      t.references :user

      t.timestamps
    end
  end
end
